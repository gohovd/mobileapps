package bignerdranch.com.listdetailview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailViewActivity extends AppCompatActivity {

    //The KEY, in the customary KEY-VALUE pair that is an EXTRA.
    private final static String EXTRA_ACTOR = "com.bignerdranch.ListDetailView.Actor";
    //The declared variable in which I meant to store the name of the Actor object that
    //was supposed to be sent along with the intent from ListViewActivity.
    private String mName;
    private static Actor mActor;

    private TextView nameText, descriptionText, pseudonymText;
    private ImageView mImageView;

    public static Intent newIntent(Context packageContext, String name) {
        Intent i = new Intent(packageContext, DetailViewActivity.class);
        i.putExtra(EXTRA_ACTOR, name);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        //Collect the name from the EXTRA.
        mName = getIntent().getStringExtra(EXTRA_ACTOR);

        mImageView = (ImageView) findViewById(R.id.imageView);
        nameText = (TextView) findViewById(R.id.nameText);
        pseudonymText = (TextView) findViewById(R.id.pseudonymText);
        descriptionText = (TextView) findViewById(R.id.descriptionText);

        //Update the Views.
        update();

    }


    public static void setActor(Actor actor) {
        mActor = actor;
    }


    private void update(){
        //ImageView.setImageResource() takes an int as parameter, seeing as all the images
        //I had stored in drawable were not numbers, I had to do a workaround.
        String imagePath = mActor.getAlias().toLowerCase();
        int id = getResources().getIdentifier(imagePath, "drawable", getPackageName());
        mImageView.setImageResource(id);

        //Update name, pseudonym/alias and description via getter methods in Actor.
        nameText.setText(mActor.getName());
        pseudonymText.setText(mActor.getAlias());
        descriptionText.setText(mActor.getDescription());
    }
}
