package bignerdranch.com.listdetailview;

import java.util.ArrayList;

/**
 * Represents an actor from the 2003 movie 'The Room'.
 */
public class Actor {

    private String name, alias, description;
    private Actor[] actors;
    private ArrayList<Actor> actorArray;

    /**
     * Constructor for class Actor.
     *
     * @param name - The real name of the actor who played in 'The Room'.
     * @param alias - i.e. pseudonym/screen-name in the movie.
     * @param description - Short description of the real or fictional person.
     */
    public Actor(String name, String alias, String description) {
        this.name = name;
        this.alias = alias;
        this.description = description;
    }
    
    public Actor(){}

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getDescription() {
        return description;
    }

    public void generateActors() {
        //Declaring variables!
        Actor tommy, juliette, greg, philip, carolyn, robyn, scott, dan, kyle;

        //Initializing objects!
        tommy = new Actor("Tommy Wiseau", "Johnny", "As the writer, director and lead actor; he was without " +
                "a doubt the star of the movie. He plays the part of concerned, hard-working father and " +
                "partner, depicting emotional states ranging from manically depressive to ecstatic - with frightening precision.");
        juliette = new Actor("Juliette Danielle", "Lisa", "Despite losing the initial round of casting, Juliette ended up giving" +
                " the performance of her life. She will forever be remembered as the aunt, or uncle - of all deceiving housewives.");
        greg = new Actor("Greg Sestero", "Mark", "In the movie, Mark doesn't reveal much. Can he act? Yes. Do we know exactly" +
                " what he's trying to act-up? Not at all. All I can say is that his lack of backstory, assertment and " +
                "enthusiasm in general really shines through and takes the movie to another level of interesting(?)");
        philip = new Actor("Philip Haldiman", "Danny", "Plays the role as Johnny's soon-to-be adoptive son. " +
                "Sure, the guy is pushing 30 but he really does make a convincing 16 year old.");
        carolyn = new Actor("Carolyn Minnot", "Claudette", "Lisa's mom.");
        robyn = new Actor("Robyn Paris", "Michelle", "Mike's girlfriend.");
        scott = new Actor("Scott Holmes", "Mike", "Michelle's boyfriend. Amazing facial expressions.");
        dan = new Actor("Dan Janjigian", "Chris", "Danny's dealer.");
        kyle = new Actor("Kyle Vogt", "Peter", "Is a psychologist.");

        //Adding all objects to an array of actors.
        actors = new Actor[]{tommy, juliette, greg, philip, dan, carolyn, robyn, scott, kyle};

        actorArray = new ArrayList<>();

        //Here I iterate through the array of actors I made, and put each of them into ArrayList.
        for (int i = 0; i < actors.length; i++) {
            actorArray.add(actors[i]);
        }
    }

    /**
     * Method for retrieving the names (String name) of all actors in the ArrayList,
     * as an ArrayList
     *
     * @return ArrayList
     */
    public ArrayList<String> getNames(){
        ArrayList<String> names = new ArrayList<>();
        for(Actor a : actorArray) {
            names.add(a.getName());
        }
        return names;
    }

    /**
     * Takes the name of an actor as parameter, and uses this input to find the actor object
     * related to this name, and returns it to the caller.
     * @param name - String; the name of the actor you want returned.
     * @return Actor
     */
    public Actor getActor(String name){
        Actor returnActor = null;
        for(Actor a : actorArray) {
            if(a.getName().equals(name)){
                returnActor = a;
            }
        }
        return returnActor;
    }
}
