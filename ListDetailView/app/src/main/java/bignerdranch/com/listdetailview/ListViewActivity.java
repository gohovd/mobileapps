package bignerdranch.com.listdetailview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewActivity extends Activity {

    //I instantiate an object of class Actor to make use of the class' methods.
    private Actor actorInstance = new Actor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        //Initialize the ListView, with @+id/listView.
        final ListView listview = (ListView) findViewById(R.id.listView);

        //Initializes all actors and puts them into an ArrayList of actors, in class Actor.
        actorInstance.generateActors();

        //Initialize adapter to manage the data model.
        final StableArrayAdapter adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, actorInstance.getNames());
        //Connect my ListView to the StableArrayAdapter.
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                //Collects the item at the position the user put his finger.
                final String item = (String) parent.getItemAtPosition(position);


                /* I was hoping to launch DetailViewActivity with an instance of Actor as the extra,
                 * but I had a hard time making that work, so I chose to simply send the String
                 * representation of the object (item.toString()) - and then the instance of Actor
                 * with the next line.
                */
                Intent i = DetailViewActivity.newIntent(ListViewActivity.this, item.toString());
                DetailViewActivity.setActor(actorInstance.getActor(item.toString()));
                startActivity(i);


            }

        });
    }

    /**
     * A minor extension of the ArrayAdapter class.
     * The methods getItemId() and hasStableIds() have been overridden in order to adapt the
     * adapter to the individual entries in the widget.
     */
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  ArrayList<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

}

