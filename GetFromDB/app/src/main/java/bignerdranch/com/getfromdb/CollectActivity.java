package bignerdranch.com.getfromdb;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CollectActivity extends AppCompatActivity {

    private TextView tvData; //Declare TextView field.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect); //Connect class CollectActivity to XML activity_collect.

        Button btnHit = (Button)findViewById(R.id.btnHit); //Connect Button with its xml-id.
        tvData = (TextView)findViewById(R.id.JsonItem); //Connect textView with its xml-id.

        //Listen for clicks on the button.
        btnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //OnClick, send a URL in String format to method in inner class JSONTask.
                new JSONTask().execute("http://catfacts-api.appspot.com/api/facts");
            }
        });
    }

    //Class for executing code in background thread, extends AsyncTask
    //which is the class that makes it possible to run code in the background.
    public class JSONTask extends AsyncTask<String, String, String>{

        //All code in this method will be executed in the background (a different thread).
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null; //Initialize HttpURLConnection
            BufferedReader reader = null; //Initialize BufferedReader

            try {
                URL url = new URL(params[0]); //Collect URL (String) from first position in params.
                connection = (HttpURLConnection) url.openConnection();
                connection.connect(); //Connect to the URL.

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    //Read lines as they come from the URL.
                    buffer.append(line);
                }

                //Use the power of JSON to collect only the String we're interested in,
                //and ignore the other overhead (the symbols {[]} )
                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                //The key to which the String we're interested in is connected.
                String fact = parentObject.getString("facts");
                //Had to trim off a couple of symbols at the beginning and end, by substring.
                return fact.substring(2, fact.length()-2);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            //After everything is nice and finished up in the background,
            //we can finally display the text in the TextView.
            tvData.setText(result);
        }

    }
}


