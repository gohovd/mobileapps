GetFromDB aka "CatFacts" -- Version 1.0 -- 17/03/2016

GOOD TO KNOW
------------
All testing of this application was done with a Lollipop 5.0 Nexus emulator.


GENERAL USAGE
-------------
Run the application, click the only available button, go on to read interesting and meaningful facts about both small and large cats.


WHAT IT DOES
-------------
Collects facts about cats from a database, and displays them to the user.

