package aalesund.ntnu.cakeornot;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Gøran on 21/04/2016.
 */
public class Flickr extends Observable {

    private static final String TAG = "Flickr";

    private static final String API_KEY = "0f9581405ad719f897e77f6e1313d159";
    private static final String PREFIX = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=" + API_KEY + "&tags=";
    private static final String SUFFIX = "&per_page=500&page=1&format=json&nojsoncallback=1";

    private static ConcurrentHashMap<String, String> imageUrlHashMap;

    private int imageUrlGenProgress = 0;

    public Flickr(String searchTerm) {
        imageUrlHashMap = new ConcurrentHashMap<>();

        //Handle search term, f.ex allow multiple terms.
        new GetJSONResponse().execute(PREFIX + processSearchTerm(searchTerm) + SUFFIX);
    }

    private String processSearchTerm(String searchTerm) {
        String[] split = searchTerm.split(" ");
        if(split.length == 1) {
            return searchTerm;
        } else {
            String finalSearchTerm = "";
            for(String s : split) {
                //Build search term.
                if(finalSearchTerm.equals("")) {
                    finalSearchTerm += s;
                } else {
                    finalSearchTerm += "+" + s;
                }
            }
            Log.i(TAG, "Search term processed: " + finalSearchTerm);
            return finalSearchTerm;
        }
    }

    public class GetJSONResponse extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpsURLConnection con = null; //Initialize HttpURLConnection
            BufferedReader reader = null; //Initialize BufferedReader
            String finalJson = "";

            try {
                URL url = new URL(params[0]); //Collect URL (String) from first position in params.
                con = (HttpsURLConnection) url.openConnection();
                con.connect(); //Connect to the URL.

                InputStream stream = con.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    //Read lines as they come from the URL.
                    buffer.append(line);
                }

                //Use the power of JSON to collect only the String we're interested in,
                //and ignore the other overhead (the symbols {[]} )
                finalJson = buffer.toString();
                JSONObject parent = new JSONObject(finalJson).getJSONObject("photos");
                JSONArray jsonArray = parent.getJSONArray("photo");

                generateImageUrls(jsonArray);

            } catch (Exception e) {
                Log.e(TAG, "Exception caught. Message: " + e);
            } finally {
                if (con != null) {
                    con.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return finalJson;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.i(TAG, "Number of URL's: " + imageUrlHashMap.values().size());
        }
    }

    /**
     * Takes a JSONArray as input and for each entry in the array, we extract details of the
     * entry needed to construct the proper url for each picutre. The url is then stored in
     * a hashmap for later retrieval.
     *
     * @param array JSONArray
     */
    private void generateImageUrls(JSONArray array) {
        String PREFIX = "https://farm";
        String SUFFIX = ".static.flickr.com/";
        String JPG = ".jpg";
        String SLASH = "/";
        String UNDERSCORE = "_";


        JSONObject instance = null;

        String id, farm, secret, server, owner, url;

        try {
            for (int i = 0; i < array.length(); i++) {
                instance = array.getJSONObject(i);

                id = instance.getString("id");
                farm = instance.getString("farm");
                secret = instance.getString("secret");
                server = instance.getString("server");
                owner = instance.getString("owner");

                // Put together the URL for the photo.
                url = PREFIX + farm + SUFFIX + server + SLASH + id + UNDERSCORE + secret + JPG;
                imageUrlHashMap.put(owner, url);

                //Progressbar in LandingActivity needs this value to display progress.
                progress(i, array.length());
                setChanged();
                notifyObservers();
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception caught, message: " + e);
        }
    }

    // This method is needed to convert between int and double. The progressbar needs
    // an int but progress is represented as decimal value.
    private void progress(int fraction, int total) {
        double f = (double) fraction + 1;
        double t = (double) total;

        double p =  (f / t) * 100;
        imageUrlGenProgress = (int) p;
    }

    public int getImageUrlGenProgress() {
        return imageUrlGenProgress;
    }

    public static String getRandomImageURL() {
        List<String> valuesList = new ArrayList<>(imageUrlHashMap.values());
        int randomIndex = new Random().nextInt(valuesList.size());
        String randomUrl = valuesList.get(randomIndex);

        return randomUrl;
    }

    public static int getRandomImageURLindex() {
        List<String> valuesList = new ArrayList<>(imageUrlHashMap.values());
        int randomIndex = new Random().nextInt(valuesList.size());

        return randomIndex;
    }

    public static String getUrl(int index) {
        List<String> valuesList = new ArrayList<>(imageUrlHashMap.values());

        return valuesList.get(index);
    }

    public static int getUrlAmount() {
        return imageUrlHashMap.values().size();
    }

    public static void removeUrlFromHashmap(String value) {
        imageUrlHashMap.values().removeAll(Collections.singleton(value));
    }
}
