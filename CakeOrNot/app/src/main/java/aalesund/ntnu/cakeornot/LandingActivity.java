package aalesund.ntnu.cakeornot;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

public class LandingActivity extends AppCompatActivity implements Observer {

    private static final String TAG = "LandingActivity";

    private ProgressBar pb;
    private Button startButton;
    private EditText searchText;
    private Flickr flickr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        searchText = (EditText) findViewById(R.id.search_text);

        startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionmenu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        pb.setProgress(0);
        searchText.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void update(Observable observable, Object data) {
        pb.setProgress(flickr.getImageUrlGenProgress());
        if(pb.getProgress() == 100) {
            Intent intent = new Intent(this, SwipeActivity.class);
            intent.putExtra(TAG, searchText.getText().toString());
            startActivity(intent);
        }
    }

    private void start() {
        if (searchText.getText().length() < 1) {
            Toast.makeText(this, "Type what you want!", Toast.LENGTH_SHORT).show();
        } else {
            flickr = new Flickr(searchText.getText().toString());
            flickr.addObserver(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action_favorite:
                // Favorite here means home aka go-to-profile.
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}


