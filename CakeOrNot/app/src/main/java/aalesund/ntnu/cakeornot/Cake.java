package aalesund.ntnu.cakeornot;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Observable;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Gøran on 13/04/2016.
 */
public class Cake extends Observable {

    private static final String TAG = "Cake";

    private Bitmap img;
    private String imageUrl;

    public Cake(String imageUrl) {
        this.imageUrl = imageUrl;

        new GetImageFromUrl().execute(imageUrl);
    }

    public Bitmap getImg() {
        return img;

    }

    public String getImageUrl() {
        return imageUrl;
    }

    private void setImg(Bitmap img) {
        this.img = img;
        setChanged();
        notifyObservers();
    }

    public void setImageUrl(int index) {
        this.imageUrl = Flickr.getUrl(index);
    }

    public class GetImageFromUrl extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            Bitmap image;
            try {
                URL url = new URL(params[0]);

                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                image = BitmapFactory.decodeStream(inputStream);
                setImg(image);

            } catch (Exception e) {
                Log.e(TAG, "Exception caught, message: " + e);
                e.printStackTrace();
            }

            return null;
        }
    }

}
