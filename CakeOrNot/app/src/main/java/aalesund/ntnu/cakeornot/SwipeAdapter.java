package aalesund.ntnu.cakeornot;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Gøran on 20/04/2016.
 */
public class SwipeAdapter extends FragmentStatePagerAdapter {

    SparseArray<CakeFragment> registeredFragments = new SparseArray<>();

    private static final String TAG = "SwipeAdapter";

    public SwipeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Log.i(TAG, "Adapter collecting index: " + i);

        return CakeFragment.newInstance(i);
    }


    @Override
    public int getCount() {
        return Flickr.getUrlAmount();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        CakeFragment fragment = (CakeFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }



}
