package aalesund.ntnu.cakeornot;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class SwipeActivity extends ActionBarActivity {

    private static final String TAG = "SwipeActivity";
    private Button infoButton, yesButton, noButton;
    private FragmentManager fm;
    private Fragment fragment;
    private SwipeAdapter swipeAdapter;
    protected int currentPosition;
    protected int previousPosition = -1;
    protected Random random = new Random();

    private static ArrayList<Cake> cakes = new ArrayList<>();

    private ViewPager mViewPager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionmenu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        previousPosition = -1;

        Toast.makeText(SwipeActivity.this, "When there are no more pictures you will be sent back.", Toast.LENGTH_LONG).show();

        mViewPager = (ViewPager) findViewById(R.id.view_pager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position > previousPosition || previousPosition == -1) {
                    currentPosition = position;
                    //Dislike, left-swipe. Destroy fragment, never want to see it again.
                    if (previousPosition != -1) {
                        Flickr.removeUrlFromHashmap(Flickr.getUrl(previousPosition));
                        swipeAdapter.notifyDataSetChanged();
                    }

                    previousPosition = position;
                    Log.i(TAG, "Did not like, size of liked-cakes: " + cakes.size() + " POSITION: " + position);

                } else if (position < previousPosition || previousPosition == -1) {
                    currentPosition = position;
                    //Like! Right-swipe. Store, and view later.
                    if (previousPosition != -1) {
                        Flickr.removeUrlFromHashmap(Flickr.getUrl(previousPosition));
                        swipeAdapter.notifyDataSetChanged();
                    }



                    int imageURLindex = swipeAdapter.registeredFragments.get(previousPosition).getGeneratedRandomIndex();
                    if(imageURLindex < Flickr.getUrlAmount() - 1 || imageURLindex >= 1) {
                        cakes.add(new Cake(Flickr.getUrl(imageURLindex)));
                    }

                    Log.i(TAG, "Did like! Liked-cakes: " + cakes.size() + " POSITION: " + position);
                    previousPosition = position;
                }

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        swipeAdapter = new SwipeAdapter(getSupportFragmentManager());
        //mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mViewPager.setAdapter(swipeAdapter);
        mViewPager.setOffscreenPageLimit(0);

        // Set initial item to be the one in the middle.
        mViewPager.setCurrentItem(swipeAdapter.getCount()/2);

        fm = getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragment_container);

        infoButton = (Button) findViewById(R.id.info_button);
        yesButton = (Button) findViewById(R.id.yes_button);
        noButton = (Button) findViewById(R.id.no_button);

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do something..
                int gamble = random.nextInt(2);
                if(gamble == 1) {
                    Toast.makeText(SwipeActivity.this, "Swipe to the right if you like!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SwipeActivity.this, "Swipe to the left if you don't!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Collect URL of Image and add to gallery at profile page.

                //Mind the maths operator 'decides' the direction we're swiping in.
                if(currentPosition - 1 <= 0) {
                    onBackPressed();
                    return;
                }
                mViewPager.setCurrentItem(currentPosition--);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Discard of the image related to the cake related to the fragment...
                if (previousPosition != -1 && previousPosition != Flickr.getUrlAmount()) {
                    Flickr.removeUrlFromHashmap(Flickr.getUrl(previousPosition));
                    swipeAdapter.notifyDataSetChanged();
                }
                if(currentPosition + 1 >= Flickr.getUrlAmount()) {
                    onBackPressed();
                    return;
                }
                mViewPager.setCurrentItem(currentPosition++);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, LandingActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action_favorite:
                // Favorite here means home aka go-to-profile.
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public static ArrayList<Cake> getLikedCakes() {
        return cakes;
    }

}
