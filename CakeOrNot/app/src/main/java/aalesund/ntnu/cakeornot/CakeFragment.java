package aalesund.ntnu.cakeornot;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

/**
 * Created by Gøran on 20/04/2016.
 */
public class CakeFragment extends Fragment implements Observer {

    private static final String TAG = "CakeFragment";
    private Cake fragmentCake;
    private ImageView mImageView;
    private int generatedRandomIndex;


    public CakeFragment() {
    }

    public static CakeFragment newInstance(int index) {
        CakeFragment fragmentCake = new CakeFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragmentCake.setArguments(args);

        return fragmentCake;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        int imgIndex = bundle.getInt("index");
        if (fragmentCake == null) {
            Random rnd = new Random();
            generatedRandomIndex = rnd.nextInt(365 + imgIndex) % Flickr.getUrlAmount();
            Cake freshCake = new Cake(Flickr.getUrl(generatedRandomIndex));
            setCake(freshCake);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstance) {


        View view = inflater.inflate(R.layout.fragment_cake, container, false);

        mImageView = (ImageView) view.findViewById(R.id.cake_fragment_image_view);

        return view;
    }



    public Cake getCake() {
        return fragmentCake;
    }

    public void setCurrentImage(Bitmap image) {
        mImageView.setImageBitmap(image);
    }

    public int getGeneratedRandomIndex() {
        return generatedRandomIndex;
    }

    public void setCake(Cake fragmentCake) {
        this.fragmentCake = fragmentCake;
        this.fragmentCake.addObserver(this);
    }


    @Override
    public void update(Observable observable, Object data) {
        if(getActivity() == null) { return; }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mImageView.setImageBitmap(fragmentCake.getImg());

            }
        });
    }
}
